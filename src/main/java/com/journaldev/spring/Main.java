package com.journaldev.spring;

import java.util.Iterator;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;

import com.journaldev.spring.config.AppConfig;
import com.journaldev.spring.config.EmployeeDao;
import com.journaldev.spring.config.MahasiswaDao;
import com.journaldev.spring.config.PersonClient;
import com.journaldev.spring.model.Employee;
import com.journaldev.spring.model.Mahasiswa;
import com.journaldev.spring.model.Person;
import com.journaldev.spring.model.Produk;

public class Main {

	public static void main(String[] args) {
		AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(AppConfig.class);

		EmployeeDao client = applicationContext.getBean(EmployeeDao.class);
		MahasiswaDao cDao = applicationContext.getBean(MahasiswaDao.class);

		System.out.println("Getting list of all people:");

//		for (Person p : client.getAllPerson()) {
//			System.out.println(p);
//		}
		
		for (Produk p : client.getAllPerson()) {
			System.out.println(p.getNama_barang());
		}
		
		for(Mahasiswa mahasiswa : cDao.getAllPerson()) {
			System.out.println(mahasiswa);
		}

		System.out.println("\nGetting person with ID 2");

		Mahasiswa mahasiswa = cDao.getById("6");
		System.out.println(mahasiswa);

		System.out.println("Adding a Person");
		Person p = new Person();
		Mahasiswa m = new Mahasiswa();
		m.setEmail("ok@gmail.com");
		m.setJurusan("IF");
		m.setNama("ok");
		m.setNrp("17138138");
		HttpStatus status = cDao.addPerson(m);
		System.out.println("respon" +status);
		p.setAge(50);
		p.setFirstName("David");
		p.setLastName("Blain");
//		HttpStatus status = client.addPerson(p);
//		System.out.println("Add Person Response = " + status);

		applicationContext.close();
	}
}
