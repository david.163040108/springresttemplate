package com.journaldev.spring.model;

public class Mahasiswa {
    private String id;    
    private  String nrp;
    private String nama;
    private String email;    
    private String jurusan;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNrp() {
		return nrp;
	}
	public void setNrp(String nrp) {
		this.nrp = nrp;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getJurusan() {
		return jurusan;
	}
	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}
	@Override
	public String toString() {
		return "Mahasiswa [id=" + id + ", nrp=" + nrp + ", nama=" + nama + ", email=" + email + ", jurusan=" + jurusan
				+ "]";
	}
    
    
}
