package com.journaldev.spring.model;

public class Produk {

	private String id;
	private String nama_barang;
	private String harga;
	private String deskripsi;
	private String stok;
	private String gambar;
	private String kategori;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNama_barang() {
		return nama_barang;
	}
	public void setNama_barang(String nama_barang) {
		this.nama_barang = nama_barang;
	}
	public String getHarga() {
		return harga;
	}
	public void setHarga(String harga) {
		this.harga = harga;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getStok() {
		return stok;
	}
	public void setStok(String stok) {
		this.stok = stok;
	}
	public String getGambar() {
		return gambar;
	}
	public void setGambar(String gambar) {
		this.gambar = gambar;
	}
	public String getKategori() {
		return kategori;
	}
	public void setKategori(String kategori) {
		this.kategori = kategori;
	}
	@Override
	public String toString() {
		return "Produk [id=" + id + ", nama_barang=" + nama_barang + ", harga=" + harga + ", deskripsi=" + deskripsi
				+ ", stok=" + stok + ", gambar=" + gambar + ", kategori=" + kategori + "]";
	}

	
	
}
