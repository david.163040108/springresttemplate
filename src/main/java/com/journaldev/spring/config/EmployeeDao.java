package com.journaldev.spring.config;

import java.util.List;

import com.journaldev.spring.model.Employee;
import com.journaldev.spring.model.Person;
import com.journaldev.spring.model.Produk;

public interface EmployeeDao {
	List<Produk> getAllPerson();
	Produk getById(String id);
}
