package com.journaldev.spring.config;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.journaldev.spring.model.Mahasiswa;
import com.journaldev.spring.model.Person;

public interface MahasiswaDao {

	List<Mahasiswa> getAllPerson();

	Mahasiswa getById(String id);

	HttpStatus addPerson(Mahasiswa mahasiswa);

	void updatePerson(Mahasiswa mahasiswa);

	void deletePerson(String id);
}
