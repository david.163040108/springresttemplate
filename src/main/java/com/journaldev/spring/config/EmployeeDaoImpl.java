package com.journaldev.spring.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.journaldev.spring.model.Employee;
import com.journaldev.spring.model.Person;
import com.journaldev.spring.model.Produk;

@Service
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	RestTemplate restTemplate;

	final String ROOT_URI = "http://localhost/tugas3-rekweb-kamis16-163040108-david/produk";
	
	public List<Produk> getAllPerson() {
	ResponseEntity<Produk[]> response = restTemplate.getForEntity(ROOT_URI, Produk[].class);
		return Arrays.asList(response.getBody());
	}

	public Produk getById(String id) {
		ResponseEntity<Produk> response = restTemplate.getForEntity(ROOT_URI + "?id="+id, Produk.class);
		return response.getBody();
	}
	
//	public List<Person> getAllPerson() {
//		ResponseEntity<Person[]> response = restTemplate.getForEntity(ROOT_URI, Person[].class);
//		return Arrays.asList(response.getBody());
//
//	}

}
