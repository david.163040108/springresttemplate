package com.journaldev.spring.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.journaldev.spring.model.Mahasiswa;
import com.journaldev.spring.model.Person;
import com.journaldev.spring.model.Produk;

@Service
public class MahasiswaDaoImpl implements MahasiswaDao {

	@Autowired
	RestTemplate restTemplate;

	final String ROOT_URI = "http://localhost/rest-server/api/mahasiswa";
	
	public List<Mahasiswa> getAllPerson() {
		ResponseEntity<Mahasiswa[]> response = restTemplate.getForEntity(ROOT_URI, Mahasiswa[].class);
		return Arrays.asList(response.getBody());
	}

	public Mahasiswa getById(String id) {
		ResponseEntity<Mahasiswa> response = restTemplate.getForEntity(ROOT_URI + "/?id="+id, Mahasiswa.class);
		return response.getBody();
	}

	public HttpStatus addPerson(Mahasiswa mahasiswa) {
		ResponseEntity<HttpStatus> response = restTemplate.postForEntity(ROOT_URI, mahasiswa, HttpStatus.class);
		return response.getBody();
	}

	public void updatePerson(Mahasiswa mahasiswa) {
		// TODO Auto-generated method stub
		
	}

	public void deletePerson(String id) {
		// TODO Auto-generated method stub
		restTemplate.delete(ROOT_URI + id);

	}

}
